#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
		
    def test_read_2(self):
        s = "5943 232469\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  5943)
        self.assertEqual(j, 232469)
		
    def test_read_3(self):
        s = "384957 234\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  384957)
        self.assertEqual(j, 234)
		
    def test_read_4(self):
        s = "334 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  334)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
		
    def test_eval_5(self):
        v = collatz_eval(1,999999)
        self.assertEqual(v, 525)
	
    def test_eval_6(self):
        v = collatz_eval(342,789)
        self.assertEqual(v, 171)
		
    def test_eval_7(self):
        v = collatz_eval(875345, 237431)
        self.assertEqual(v, 525)
		
    def test_eval_8(self):
        v = collatz_eval(50001, 50230)
        self.assertEqual(v, 234)
		
    def test_eval_9(self):
        v = collatz_eval(333333, 333333)
        self.assertEqual(v, 154)
		
    def test_eval_10(self):
        v = collatz_eval(800, 1999)
        self.assertEqual(v, 182)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
		
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 342, 789, 171)
        self.assertEqual(w.getvalue(), "342 789 171\n")
		
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 875345, 237431, 525)
        self.assertEqual(w.getvalue(), "875345 237431 525\n")	
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
			
    def test_solve_2(self):
        r = StringIO("1 999999\n342 789\n298 458\n23 899\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n342 789 171\n298 458 144\n23 899 179\n")

    def test_solve_3(self):
        r = StringIO("900000 999999\n800 2\n759323 234857\n40 5\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "900000 999999 507\n800 2 171\n759323 234857 509\n40 5 112\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
